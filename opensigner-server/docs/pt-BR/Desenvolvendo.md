# Documentação para desenvolvedores

Documentação dedicada a auxiliar quem tiver interesse em contribuir com o projeto.

## Stack de Código

- [Typecript](https://www.typescriptlang.org/) - liguagem principal de desenvolvimento;
- [Node.js](https://nodejs.org/en/) - ambiente de execução de JavaScript;

### Bibliotecas importantes

> TODO: Adicionar libs utilzadas aqui

- [ESLint](https://eslint.org/) e [Prettier](https://prettier.io/) - bibliotecas de formatação e padronização de código

## Setup

### Pré-requisitos

- [Node.js](https://nodejs.org/dist/v18.18.0/) ^18.18.0 (recomendada instalação via [node version manager](https://nodejs.org/en/download/package-manager/#nvm))
- [Yarn](https://classic.yarnpkg.com/en/docs/install) 1.22.19 (é possível utilizar a versão [Yarn 2](https://yarnpkg.com/getting-started/install), mas para ser compatível com o projeto, devem ser feitas configurações extras)

**Instalar dependencias NodeJS:**

Primeiramente, devem ser instaladas as bibliotecas/dependências do projeto. Para isso, basta executar o seguinte comando:

```bash
yarn install
```

Deve ter sido adicionado o diretório `node_modules/`, dentro do respectivo sub projeto.

## Rodando o projeto localmente

Para executar o projeto localmente, basta utilizar o comando:

```bash
yarn dev
```

## Experiência de desenvolvimento

### Arquitetura

Nossa arquitetura segue os conceitos do [_Clean Code_](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). Uma arquitetura dividida em camadas onde cada uma possui responsabilidades e permissões definidas, dando maior escalabilidade ao projeto.

A principal característica desta arquitetura é que as camadas começam do centro e se tornam cada vez mais externas. Também, as camadas mais internas não devem ter ciência das mais externas.

A seguir é como se traduziram as práticas sugeridas pela Clean Architecture.

#### **1ª Camada**

**Model**

- Camada mais interna, acessível por todas as outras;
- Responsável por definir as entidades utilizadas em toda a aplicação;
- Para cada entidade, define suas regras de negócio e responsabilidades;
- Localizados em `./src/models/`;

#### **2ª Camada**

**Use Case**

- Responsável por guardar as regras de negócio da aplicação;
- Interage com as interfaces, nomeadas como `Services`, para obter dados externos;
- Interage com os repositórios para salvar localmente os dados obtidos pelos Services;
- Localizada em `./src/useCases/`;

**Datasource**

- Declara interfaces para implementações que poderão interagir com Bancos de Dados ou outras fontes de dados, como APIs externas;
- Permite os `Use Cases` interagirem com as camadas externas;
- Localizado em `./src/datasources`;

**Repository**

- Implementa as interfaces definidas em `Datasources`;
- Realiza o mapeamento dos dados entre os modelos externos e modelos internos;
- Localizado em `./src/repositories/`

#### **3ª Camada**

**Service**

- Também implementa as interfaces definidas em `Datasources`;
- Conhece modelos externos à aplicação;
- Realiza o mapeamento dos dados entre os modelos externos e modelos internos;
- Interage com APIs e Bancos de Dados externos à aplicação;
- Localizado em `./src/services/`

**Presenter**

- Conhece os modelos das interfaces das `Views` da aplicação;
- Realiza o mapeamento dos dados entre modelos consumidos pelas `Views` e modelos internos;
- Interage e gerencia as `Views` da aplicação;
- Localizado em `./src/presenters/`

**Controller**

- Conhece os modelos externos à aplicação;
- Interage com interfaces externas para receber dados;
- Interage com os `Use Cases` para executar ações;
- Declara e relaciona rotas do servidor com os respectivos tratamentos de dados executados nos `Use Cases`;
- Localizado em `./src/controllers/`

#### **4ª Camada**

**External Interface (API)**

- Declara ou instancia interfaces externas com as quais as camadas mais internas irão se comunicar;
- Localizadas em `./src/_common/apis/`

**Database _(Not relevant for this project)_**

- Instancia e estabelece a comunicação com os bancos de dados da aplicação;
- Localizadas em `./src/database/`

**View _(Not relevant for this project)_**

- Telas, páginas ou interfaces para o usuário interagir com a aplicação;
- Interage com os `UseCases` para executar ações;
- Consome dados dos `Presenters` reativamente.

## Gerando versão

> TODO: documentar o processo de geração de versões

> TODO: definir o gitflow do projeto

> TODO: documentar como utilizar o CI/CD para criar versões e como o fazer sem as automações

### Versionamento

O projeto segue as diretrizes de nomenclatura e organização do [versionamento semântico](https://semver.org/). Assim, devemos subir a versão a cada novo lançamento nos ambientes de staging e produção e é ideal que ambas sigam o mesmo nome quando forem geradas a partir do mesmo estado do repositório.

Fazemos isso gerando tags no git com o formato `vX.X.X`. O jeito correto de subir a versão é usando o script abaixo, que além de atualizar a versão no package.json e criar a git tag, ele também controla as versões nativas de Android e iOS.

```
yarn version --new-version X.X.X
```

Se não quiser explicitar a versão, existem comandos relativos:

```
yarn version --new-version patch
yarn version --new-version minor
yarn version --new-version major
```

Após o comando ser bem-sucedido, você precisa empurrar a tag e o commit pro repositório remoto:

```
git push origin
git push origin --tags
```

### Dev

### Staging

### Prod
