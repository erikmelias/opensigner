/**
 * Setup express server.
 */

import EnvVars from '@src/constants/EnvVars'
import HttpStatusCodes from '@src/constants/HttpStatusCodes'
import Paths from '@src/constants/Paths'
import { NodeEnvs } from '@src/constants/misc'
import { RouteError } from '@src/other/classes'
import BaseRouter from '@src/routes/api'
import express, { Express, NextFunction, Request, Response } from 'express'

// **** Variables **** //

const app: Express = express()

// **** Setup **** //

// Basic middleware
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
// Logger
app.use((req: Request, res: Response, next: NextFunction) => {
  console.log(`Recieved request: ${req.method} at ${req.url}`)
  next()
})

// Security
if (EnvVars.NodeEnv === NodeEnvs.Production.valueOf()) {
  // TODO: config headers
}

// Add APIs, must be after middleware
app.use(Paths.Base, BaseRouter)

// Add error handler
app.use((err: Error, _: Request, res: Response) => {
  let status = HttpStatusCodes.BAD_REQUEST
  if (err instanceof RouteError) {
    status = err.status
  }
  return res.status(status).json({ error: err.message })
})

// Example route
app.get('/', (_: Request, res: Response) => {
  return res.send('Welcome to OpenSigner!')
})

export default app
