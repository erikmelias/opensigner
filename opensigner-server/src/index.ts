import server from './server'

const PORT: number = 3000

// **** Run **** //
server.listen(PORT, () => {
  console.log(`Servidor está ouvindo na porta ${PORT}`)
})
