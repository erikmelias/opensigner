import { UserRepository } from '@src/repositories/UserRepository'
import { IUser } from '@src/models/User'
import { RouteError } from '@src/other/classes'
import HttpStatusCodes from '@src/constants/HttpStatusCodes'

// **** Variables **** //

export const USER_NOT_FOUND_ERR = 'User not found'

// **** Functions **** //

/**
 * Get all users.
 */
function getAll(): Promise<IUser[]> {
  return UserRepository.getAll()
}

/**
 * Add one user.
 */
function addOne(user: IUser): Promise<void> {
  return UserRepository.add(user)
}

/**
 * Update one user.
 */
async function updateOne(user: IUser): Promise<void> {
  const persists = await UserRepository.persists(user.id)
  if (!persists) {
    throw new RouteError(HttpStatusCodes.NOT_FOUND, USER_NOT_FOUND_ERR)
  }
  // Return user
  return UserRepository.update(user)
}

/**
 * Delete a user by their id.
 */
async function _delete(id: number): Promise<void> {
  const persists = await UserRepository.persists(id)
  if (!persists) {
    throw new RouteError(HttpStatusCodes.NOT_FOUND, USER_NOT_FOUND_ERR)
  }
  // Delete user
  return UserRepository.delete(id)
}

// **** Export default **** //

export default {
  getAll,
  addOne,
  updateOne,
  delete: _delete,
} as const
