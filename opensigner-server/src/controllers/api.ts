import { Router } from 'express'

import Paths from '../constants/Paths'
import UserRoutes from './UserRoutes'

// **** Variables **** //

const apiRouter = Router()

// ** Add UserRouter ** //

const userRouter = Router()

// TODO: Verify if definitions below are valid, changed from templete
// Get all users
userRouter.get(Paths.Users.Get, UserRoutes.getAll)

// Add one user
userRouter.post(Paths.Users.Add, UserRoutes.add)

// Update one user
userRouter.put(Paths.Users.Update, UserRoutes.update)

// Delete one user
userRouter.delete(Paths.Users.Delete, UserRoutes.delete)

// Add UserRouter
apiRouter.use(Paths.Users.Base, userRouter)

// **** Export default **** //

export default apiRouter
