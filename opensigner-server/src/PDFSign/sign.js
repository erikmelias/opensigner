
import SignPDF from "./SignPDF.js";
import fs from "node:fs";
import path from "node:path";


async function main() {
    const pdfBuffer = new SignPDF(
        //path.resolve('opensigner-server/src/PDFSign/assets/pdf_document.pdf'),
        path.resolve('opensigner-server/src/PDFSign/assets/signed_document.pdf'),
        path.resolve('opensigner-server/src/PDFSign/assets/test_certificate.p12')
        //path.resolve('assets/phaccert.pfx') /testing ICP-BRASIL
    );

    const signedDocs = await pdfBuffer.signPDF();
    const pdfName = `opensigner-server/src/PDFSign/assets/double_signed.pdf`;

    fs.writeFileSync(pdfName, signedDocs);
    console.log(`Signed PDF: ${pdfName}`);
}

main();