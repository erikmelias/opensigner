
# OpenSigner



#### Alcançando a Irrefutabilidade 



### Introdução

Com o advento de práticas voltadas à privacidade, sejam essas impostas por lei (por exemplo LGPD) ou por compliance, sendo esse último por vezes impulsionado por padrões (família ISO 27000), a assinatura eletrônica se faz necessária. Há no mercado diversas soluções para assinatura eletrônica e digital, geralmente com custo envolvido ou limitada para assinaturas mais simples com por exemplo voltadas ao consentimento, fazendo que os desenvolvedores projetem e criem toda a solução (bancos costumam fazer isso).


### Objetivo

Criação de uma ferramenta opensource que possibilite efetuar assinatura eletrônica e a facilitar a gestão de consentimento, de forma integrada a outros softwares.


### Interessados

Dentre os possíveis interessados, estão incluídas as Comunidades de desenvolvedores,  Empresas desenvolvedoras de software,  Empresas com aplicações proprietárias. Em outras palavras, os interessados são todos que possuem a necessidade de implementar assinatura eletrônica como forma de atendimento de regras, ou criação de evidências quanto ao consentimento e concordância. Pode-se citar como exemplos práticos e diretos as aplicações sites de e-commerces e financeiros ou Apps Mobiles que possuem algum tipo de transação. De modo geral, não se trata de uma solução final, portanto há a necessidade de conhecimento técnico para integrar o OpenSigner junto a outra solução, facilitando então a operação e gestão de assinaturas eletrônicas em diferentes contextos.


### Tecnologias, Recursos ou Estudos

Conforme escopo a ser definido para o tempo da disciplina, há uma série de requisitos e possibilidades, bem como o uso de tecnologias ou recursos. Além das práticas ligadas ao desenvolvimento de software, seguem alguns possíveis assuntos relacionados:



* Certificados Digital e/ou ICP-Brasil
* Pacote de rede ou cabeçalho HTTP
* Armazenamento de dados estruturados e não estruturados
* Armazenamento de dados centralizados e/ou com custo baixo em nuvem (S3, GDrive, Dropbox)
* Armazenamento distribuídos (IPFS ou outros)
* Microservicess ou Serverless
* Blockchain (contrato inteligentes)
* Integração com Carteiras digitais (carteiras de criptomoedas)
* Rastreabilidade em geral


### Notas



* A assinatura eletrônica pode ser feita tanto por certificado digital (assinatura digital) e por coleta de evidências (assinatura eletrônica), blockchain ou gerar outros rastros que evidenciam a mesma.
* O Consentimento muitas vezes não precisa ser um documento “gigante” assinado, mas precisa minimamente garantir que o determinado usuário concordou com algo.
* Podem se beneficiar estudantes com pesquisa relacionadas aos temas: Segurança em geral (Criptografia, assinatura eletrônica, ISO 27k); Web3; Desenvolvedores de Sistemas ou de integração


### Setores que podem se beneficiar deste projeto



* **Contratos e Documentos Legais:** Empresas de todas as indústrias podem usar soluções de assinatura eletrônica para facilitar a assinatura e gestão de contratos, acordos e outros documentos legais. Isso pode incluir contratos de vendas, acordos de parceria, termos de serviço e muito mais.
* **Setor Financeiro:** Instituições financeiras usam assinaturas eletrônicas para autorizar transações, abrir contas bancárias, conceder empréstimos, investir e muito mais.
* **Setor Imobiliário:** Corretores imobiliários e empresas do setor imobiliário utilizam assinaturas eletrônicas para contratos de aluguel, acordos de compra e venda, e outros documentos relacionados a propriedades.
* **Recursos Humanos:** Empresas usam sistemas de assinatura eletrônica para agilizar o processo de contratação, coleta de informações pessoais, acordos de confidencialidade e outros documentos de RH.
* **Saúde e Cuidados Médicos:** O setor de saúde pode usar assinaturas eletrônicas para obter consentimento de pacientes para tratamentos médicos, divulgação de informações médicas e outros formulários relacionados a cuidados de saúde.
* **Educação: **Instituições educacionais podem usar assinaturas eletrônicas para aceitar matrículas de alunos, autorizações de viagens escolares, formulários de consentimento para menores de idade e mais.
* **Governo e Administração Pública:** Governos podem adotar assinaturas eletrônicas para processos administrativos, licenciamento, solicitação de serviços públicos e outros procedimentos.
* **Setor de Tecnologia:** Empresas de tecnologia podem usar essas soluções para autorizar acesso a sistemas, aceitar termos de uso de software, obter aprovações internas e muito mais.
* **Seguros:** Companhias de seguros podem usar assinaturas eletrônicas para apólices, reivindicações, renovações e outros documentos relacionados a seguros.
* **Serviços de Consultoria e Jurídicos:** Consultorias, escritórios de advocacia e empresas de consultoria usam assinaturas eletrônicas para formalizar acordos com clientes e parceiros.
* **Comércio Eletrônico:** Empresas de comércio eletrônico podem usar essas soluções para processar pedidos, obter consentimento para políticas de privacidade e termos de venda, e confirmar transações.