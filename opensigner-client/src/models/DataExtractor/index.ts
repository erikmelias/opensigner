export class DataExtractor {
  private startTime: number;
  private endTime: number;
  private elapsedTime: number;
  private mousePositions: any[];

  private handleMouseMove(event: any): void {
    let eventDoc, doc, body;

    event = event || window.event; // IE-ism

    // If pageX/Y aren't available and clientX/Y are,
    // calculate pageX/Y - logic taken from jQuery.
    // (This is to support old IE)
    if (event.pageX == null && event.clientX != null) {
      eventDoc = (event.target && event.target.ownerDocument) || document;
      doc = eventDoc.documentElement;
      body = eventDoc.body;

      event.pageX = event.clientX +
          (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
          (doc && doc.clientLeft || body && body.clientLeft || 0);
      event.pageY = event.clientY +
          (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
          (doc && doc.clientTop  || body && body.clientTop  || 0 );
    }
    this.mousePositions.push({positionX: event.pageX, positionY: event.pageY});
  }

  public constructor() {
    this.startTime = new Date().getTime();
    this.endTime = 0;
    this.elapsedTime = 0;
    this.mousePositions = [];
    document.onmousemove = this.handleMouseMove;
  }

  public finish(): void {
    this.endTime = new Date().getTime();
    this.elapsedTime = this.endTime - this.startTime;
    document.onmousemove = null;
  }

  public time(): number {
    return this.elapsedTime;
  }
}

export default DataExtractor;