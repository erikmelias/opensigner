import axios from 'axios';

// **** Functions **** //

export type SessionData = {
    cookies: any,
    localStorage: any,
    navigator: any,
    document: any,
    geolocalization: any,
}
export class SessionDataCollector {

  public constructor() {

  }

  public async collect(): Promise<SessionData> {
    const sessionData: SessionData = {
      cookies: document.cookie,
      localStorage: window.localStorage,
      navigator: navigator,
      document: document,
      geolocalization: (await axios.get('https://api.ipify.org?format=json')).data.data.ip,
    }
    return sessionData;
  }
}

export default SessionDataCollector;