import DataCollector from './models/DataCollector';
import DataExtractor from './models/DataExtractor';

export class OpenSigner {
  private collector: DataCollector;
  private extractor: DataExtractor;

  public constructor() {
    this.collector = new DataCollector();
    this.extractor = new DataExtractor();
  }

  public generate(): any {
    const data = {
      sessionData: this.collector.collect(),
      extractedData: this.extractor.time(),
    }
    return data;
  }
}

export default OpenSigner;